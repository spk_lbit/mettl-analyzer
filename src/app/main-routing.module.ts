import { ResultPageComponent } from './pages/result-page/result-page.component';
import { ParsePageComponent } from './pages/parse-page/parse-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';
import { NotfoundPageComponent } from './pages/notfound-page/notfound-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'parse',
    pathMatch: 'full'
  },
  {
    path: 'parse',
    component: ParsePageComponent

  },
  {
    path: 'result',
    component: ResultPageComponent

  },
  {
    path: 'auth',
    component: AuthPageComponent
  },
  {
    path: '**',
    component: NotfoundPageComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
