import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MainRoutingModule } from './main-routing.module';
import { RootComponent } from './root/root.component';
import { ResultPageComponent } from './pages/result-page/result-page.component';
import { ParsePageComponent } from './pages/parse-page/parse-page.component';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';
import { NotfoundPageComponent } from './pages/notfound-page/notfound-page.component';

@NgModule({
  declarations: [
    RootComponent,
    ResultPageComponent,
    ParsePageComponent,
    AuthPageComponent,
    NotfoundPageComponent
  ],
  imports: [
    BrowserModule,
    MainRoutingModule
  ],
  providers: [],
  bootstrap: [RootComponent]
})
export class MettlAnalyzer { }
